[Transient Analysis]
{
   Npanes: 1
   {
      traces: 2 {524290,0,"V(v_out)"} {589828,0,"V(v_src)"}
      X: ('m',0,0,0.005,0.05)
      Y[0]: (' ',1,-0.5,0.5,5)
      Y[1]: (' ',0,1e+308,9,-1e+308)
      Volts: (' ',0,0,1,-0.5,0.5,5)
      Log: 0 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
[AC Analysis]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"V(v_out)"}
      X: ('K',0,10,0,10000)
      Y[0]: (' ',0,0.0158489319246111,4,1.58489319246111)
      Y[1]: (' ',0,-90,9,0)
      Log: 1 2 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
