[Transient Analysis]
{
   Npanes: 1
   {
      traces: 2 {524290,0,"V(v_out)"} {589828,0,"V(v_src)"}
      X: ('m',0,0,0.003,0.03)
      Y[0]: (' ',1,0,0.5,5)
      Y[1]: (' ',0,1e+308,9,-1e+308)
      Volts: (' ',0,0,1,0,0.5,5)
      Log: 0 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
